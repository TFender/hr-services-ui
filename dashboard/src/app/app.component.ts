import { Component } from '@angular/core';
import {RancherList} from "./rancher/rancherList";
import {Rancher} from "./rancher/rancher";
import {Gitlab} from "./gitlab/gitlab";
import {GitlabList} from "./gitlab/gitlabList";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  rancherList: Rancher[]
  gitlabList: Gitlab[]
  status: string = ''

  constructor() {
    this.rancherList = new RancherList().getList();
    this.gitlabList = new GitlabList().getList();
  }

  getStatus(event){
    this.status = event
  }
}
