/**
 * Created by dbxwtk on 5-1-2018.
 */
import {Gitlab} from "./gitlab";
export class GitlabList {
  private list: Gitlab[] = []

  vestiging: Gitlab = new Gitlab('1378','hr-vestiging');
  maatschappelijkeactiviteit:  Gitlab = new Gitlab('1401','hr-maatschappelijke-activiteit');
  persoon:  Gitlab = new Gitlab('1490','hr-persoon');
  locatie: Gitlab = new Gitlab('1499','hr-locatie');
  vestigingplus:  Gitlab = new Gitlab('1513','hr-vestiging-informatie-service');
  maplus:  Gitlab = new Gitlab('1528','hr-maatschappelijke-activiteit-informatie-service');
  hrservicesorch:  Gitlab = new Gitlab('1516','hr-services-orchestrator');
  hrservicestest:  Gitlab = new Gitlab('1404','hr-services-test');
  htservicesui:  Gitlab = new Gitlab('1545','hr-services-ui');

  constructor () {
    this.list.push(this.vestiging);
    this.list.push(this.maatschappelijkeactiviteit);
    this.list.push(this.persoon);
    this.list.push(this.locatie);
    this.list.push(this.vestigingplus);
    this.list.push(this.maplus);
    this.list.push(this.hrservicesorch);
    this.list.push(this.hrservicestest);
    this.list.push(this.htservicesui);
  }

  public getList(): Gitlab[]
  {
    return this.list;
  }

}
