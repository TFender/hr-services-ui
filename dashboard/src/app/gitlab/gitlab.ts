/**
 * Created by dbxwtk on 5-1-2018.
 */
export class Gitlab {
  url: string = 'http://git.k94.kvk.nl/api/v4/projects/';
  projectid: string;
  id: string = null;
  service: string;

  constructor(projectid:string, service:string){
    this.projectid = projectid;
    this.service = service;
  }

  public setId(id: string): void{
    this.id = id;
  }

}
