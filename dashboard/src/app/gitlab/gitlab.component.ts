import {Component, Input, EventEmitter, Output} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Gitlab} from "./gitlab";

@Component({
  selector: 'gitlab',
  templateUrl: './gitlab.component.html',
  styleUrls: ['./gitlab.component.css']
})

export class GitlabComponent {
  @Input() object: Gitlab = null;
  @Output() status: EventEmitter<any> = new EventEmitter();
  info: string = 'info';
  json: any = null;
  jobs: any = null;
  url: string;
  private token: string = 'GmG2hKDoDuH6ACzRXfvJ';
  data: any = [''];
  hover = null;
  isCalling: boolean = false;

  constructor(private http: HttpClient) {

  }

  ngOnInit(): void {
    this.doCall();
    setInterval(() => {
      this.doCall();
    }, 5000);
  }

  doCall() {
    this.isCalling = true;
    this.http.get(this.object.url + this.object.projectid + '/pipelines', {headers: {'Private-Token': this.token}})
      .subscribe(data => {
        this.http.get(this.object.url + this.object.projectid + '/pipelines/' + data[0].id, {headers: {'Private-Token': this.token}})
          .subscribe(pipeline => {
            this.json = pipeline;
            this.http.get(this.object.url + this.object.projectid + '/pipelines/' + data[0].id + '/jobs', {headers: {'Private-Token': this.token}})
              .subscribe(jobs => {
                this.jobs = jobs;
                this.getData(this.jobs)
                this.jobs = this.compareJobs(this.jobs);
                this.isCalling = false;
              }, (err) => {
                console.log('error');
                this.isCalling = false;
              });
          }, (err) => {
            console.log('error');
            this.isCalling = false;
          });
      }, (err) => {
        console.log('error');
        this.isCalling = false;
      });
  }

  setSelect(item) {
    item ? this.hover = item.name + ' | ' + item.status + ' | ' + Number(item.duration).toFixed(2) + ' sec' : this.hover = '';
    this.status.emit(this.hover);
  }

  compareJobs(jobs) {
    for (var i = 0; i < jobs.length; i++) {
      for (var j = (i + 1); j < jobs.length; j++) {
        if (jobs[i].name == jobs[j].name && i != j) {
          jobs[i] = jobs[j];
          jobs.splice(j, 1);
        }
      }
    }
    return jobs;
  }

  getData(list): void {
    var dataList: any[] = list.sort((n1, n2) => {
      if (n1.id > n2.id) {
        return 1;
      }
      if (n1.id < n2.id) {
        return -1;
      }
      return 0;
    });
    this.data = dataList;
  }

  getColor(item) {
    if (item) {
      switch (item.status) {
        case 'success' :
          return '#8BC68D';
        case 'failed' :
          return item.name.indexOf('*') != -1 ? '#FFD387' : '#FF7674';
        case 'skipped' :
          return '#9fa0a8';
        case 'running' :
          return '#78b0ff';
      }
      return '#6f75a5';
    }
  }

  getIcon(item) {
    if (item) {
      switch (item.status) {
        case 'success' :
          return 'fa-check';
        case 'failed' :
          return item.name.indexOf('*') != -1 ? 'fa-exclamation' : 'fa-times';
        case 'skipped' :
          return 'fa-minus';
        case 'running' :
          return 'fa-spinner fa-pulse';
      }
    }
  }

  getStyle(data) {
    return "width: " + (100 / data.length) + "%";
  }

}
