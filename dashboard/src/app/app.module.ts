import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {RancherComponent} from "./rancher/rancher.component";
import {HttpClientModule} from "@angular/common/http";
import {HttpClient} from "@angular/common/http";
import {GitlabComponent} from "./gitlab/gitlab.component";

@NgModule({
  declarations: [
    AppComponent,
    RancherComponent,
    GitlabComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
  ],
  providers: [HttpClient],
  bootstrap: [AppComponent]
})
export class AppModule { }
