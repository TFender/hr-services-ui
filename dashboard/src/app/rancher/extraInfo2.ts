/**
 * Created by dbxwtk on 8-1-2018.
 */
export class ExtraInfo2 {
  Environment: string

  constructor(Environment: string){
    this.Environment = Environment;
  }

  public toString = () : string => {
    return 'Environment: ' + this.Environment;
  }
}
