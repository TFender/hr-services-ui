/**
 * Created by dbxwtk on 5-1-2018.
 */
import {Rancher} from "./rancher";
import {ExtraInfo} from "./extraInfo";
import {ExtraInfo2} from "./extraInfo2";
export class RancherList {
  private list: Rancher[] = []

  envid1: string = '1a2535024';
  envid2: string = '1a2535144';
  envidmon: string = '1a2535213';

  accesskey1: string = '814BD457C33AFF6AC475';
  accesskey2: string = '6961D9CB10CA7D34CA30';
  accesskeymon: string = 'C4CC128E2BDA03F77317';

  secretkey1: string = 'aSEXDoQKJETKFNppmKZaStSYBr1puKyBTgnghNMf';
  secretkey2: string = 'hpGJk1PMhZfTWQd1vuB5pQXHaLXKzVrDyfv4hb6F';
  secretkeymon: string = 'u7BgwagXc1u1jgHek1SGAGBgHZDzmeqfGv5Bq49p';

  vestiging: Rancher = new Rancher(this.envid1,this.accesskey1,this.secretkey1,'hr-vestiging');
  maatschappelijkeactiviteit: Rancher = new Rancher(this.envid1,this.accesskey1,this.secretkey1,'hr-maatschappelijke-activiteit');
  persoon: Rancher = new Rancher(this.envid1,this.accesskey1,this.secretkey1,'hr-persoon');
  locatie: Rancher = new Rancher(this.envid2,this.accesskey2,this.secretkey2,'hr-locatie');
  vestigingplus: Rancher = new Rancher(this.envid2,this.accesskey2,this.secretkey2,'hr-vestiging-informatie');
  maplus: Rancher = new Rancher(this.envid2,this.accesskey2,this.secretkey2,'hr-maatschappelijke-activiteit-informatie');
  test: Rancher = new Rancher(this.envidmon,this.accesskeymon,this.secretkeymon,'hr-services-test');

  constructor () {
    this.vestiging.addExtraInfo(new ExtraInfo('npo-hr-services','8080'));
    this.list.push(this.vestiging);

    this.maatschappelijkeactiviteit.addExtraInfo(new ExtraInfo('npo-hr-services','8081'))
    this.list.push(this.maatschappelijkeactiviteit);

    this.persoon.addExtraInfo(new ExtraInfo('npo-hr-services','8082'))
    this.list.push(this.persoon);

    this.locatie.addExtraInfo(new ExtraInfo('npo-hr-services-2','8083'))
    this.list.push(this.locatie);

    this.vestigingplus.addExtraInfo(new ExtraInfo('npo-hr-services-2','8084'))
    this.list.push(this.vestigingplus);

    this.maplus.addExtraInfo(new ExtraInfo('npo-hr-services-2','8085'))
    this.list.push(this.maplus);

    this.test.addExtraInfo2(new ExtraInfo2('npo-hr-services-monitoring'))
    this.list.push(this.test);
  }

  public getList(): Rancher[]
  {
    return this.list;
  }

}
