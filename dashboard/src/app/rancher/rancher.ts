import {ExtraInfo} from "./extraInfo";
import {ExtraInfo2} from "./extraInfo2";
/**
 * Created by dbxwtk on 5-1-2018.
 */
export class Rancher {
  url : string = 'rancher.core.k94.kvk.nl/v2-beta/projects/'
  envid : string
  accesskey : string
  secretkey: string
  service: string
  extrainfo: ExtraInfo
  extrainfo2: ExtraInfo2

  constructor(envid: string, accesskey:string, secretkey:string, service:string){
    this.envid = envid;
    this.accesskey = accesskey;
    this.secretkey = secretkey;
    this.service = service;
  }

  addExtraInfo(extrainfo: ExtraInfo){
    this.extrainfo = extrainfo;
  }

  addExtraInfo2(extrainfo2: ExtraInfo2){
    this.extrainfo2 = extrainfo2;
  }

}
