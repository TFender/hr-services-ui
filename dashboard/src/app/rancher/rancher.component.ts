/**
 * Created by dbxwtk on 5-1-2018.
 */
import {Rancher} from "./rancher";
import {Component, Input, Output, EventEmitter} from "@angular/core";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'rancher',
  templateUrl: './rancher.component.html',
  styleUrls: ['./rancher.component.css']
})
export class RancherComponent {
  @Input() object: Rancher = null;
  @Output() status: EventEmitter<any> = new EventEmitter();
  info: string = 'info';
  json: any = '';
  url: string;
  isCalling: boolean = false;
  hover: string = '';

  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {
    this.url = 'http://'+ this.object.url + this.object.envid + '/stacks/?name=' + this.object.service;
    this.doCall();
    setInterval(() => {
      this.doCall();
    }, 5000);
  }

  doCall(): any {
    this.isCalling = true;
    this.http.get('http://localhost:3000/rancher/' + this.object.envid + '/' + this.object.service + '/' + this.object.accesskey + '/' + this.object.secretkey)
      .subscribe(data => {
          if (data) {
            this.json = this.getData(data);
            this.isCalling = false;
          }
        },
        (err) => {
        console.log('error');
        this.isCalling = false;
        });
  }

  setSelect(item) {
    item ? this.hover =  'State : ' + item.state + ' | health: ' + item.healthState : this.hover = '';
    this.status.emit(this.hover);
  }

  getColor(item) {
    if (item) {
      switch (item.state || item.healthState) {
        case 'active' :
          return '#8BC68D';
        case 'healthy' :
          return '#8BC68D';
        case 'upgraded' :
          return '#FFD387';
        case 'inactive' :
          return '#FF7674';
      }
    }
    return '#9fa0a8';
  }

  getIcon(item) {
    if (item) {
      switch (item.state || item.healthState) {
        case 'healthy' :
          return 'fa-check';
        case 'active' :
          return 'fa-check';
        case 'upgraded' :
          return 'fa-exclamation-triangle';
        case 'inactive' :
          return 'fa-times';
      }
    }
    return 'fa-question';
  }

  getData(data) {
    return data.data[0];
  }

}
