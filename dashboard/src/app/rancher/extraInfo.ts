/**
 * Created by dbxwtk on 8-1-2018.
 */
export class ExtraInfo {
  Environment: string
  LocalPort: string

  constructor(Environment: string, LocalPort: string){
    this.Environment = Environment;
    this.LocalPort = LocalPort;
  }

  public toString = () : string => {
    return 'Environment: ' + this.Environment + ', Local port: ' + this.LocalPort;
  }
}
