var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var http = require('http');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var port = process.env.PORT || 3000;

var router = express.Router();

router.get('/:envid/:service/:access/:secret', function (req, res) {
  var envid = req.params.envid;
  var service = req.params.service;
  var access = req.params.access;
  var secret = req.params.secret;
  var request = http.request(getOptions(envid, service, access, secret), function (resp) {
    var body = '';
    resp.on('data', function (d) {
      body += d;
    });
    resp.on('end', function () {
      var parsed = JSON.parse(body)
      var response = ""
      if (parsed.data && parsed.data.length>0)
      {response = getStatus(envid, parsed.data[0].id, access, secret, function (data) {
        res.json(data);
      }); }
      else {response = getStatus(envid, parsed.data.id, access, secret, function (data) {
        res.json(data);
      }); }
    });
  });
  request.on('error', function (err) {
    res.end('no response');
  });
  request.end();
});



function getStatus(envid, id, access, secret, response){
  var body = '';
  var request = http.request(getStackOptions(envid, id, access, secret), function (resp) {
    resp.on('data', function (d) {
      body += d;
    });
    resp.on('end', function () {
      var parsed = JSON.parse(body);
      response(parsed);
    });
  });
  request.on('error', function (err) {
    console.log(err);
    req.end('no response');
  });
  request.end();
}


function getOptions(envid, service, access, secret) {
  var options = {
    host: 'rancher.core.k94.kvk.nl',
    path: '/v2-beta/projects/' + envid + '/stacks/?name=' + service,
    method: 'GET',
    headers: getHeaders(access, secret)
  };
  return options;
}

function getStackOptions(envid, id, access, secret) {
  var options = {
    host: 'rancher.core.k94.kvk.nl',
    path: '/v2-beta/projects/' + envid + '/stacks/'+id+'/services/',
    method: 'GET',
    headers: getHeaders(access, secret)
  };
  return options;
}

function getHeaders(access, secret) {
  var headers = {
    'Authorization': 'Basic ' + new Buffer(access + ":" + secret, 'utf8').toString('base64'),
    'Accept': 'application/json',
    'Accept-Charset': 'utf-8',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'
  };
  return headers;
}

app.use('/rancher', router);
app.listen(port);
console.log('Go to localhost:' + port);


