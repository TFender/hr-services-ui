FROM node:latest

COPY dashboard /dashboard

RUN npm config set proxy http://proxy1.kvk.nl:8080
RUN npm config set https-proxy http://proxy1.kvk.nl:8080
WORKDIR /dashboard
RUN npm install
RUN npm run build
